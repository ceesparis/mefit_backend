# MeFitFrontend

This is the backend repository for the MeFit application API. 
For further information visit the [frontend repository](https://gitlab.com/ceesparis/mefit_frontend).

## Maintainers

* [@ceesparis](https://gitlab.com/ceesparis)
* [@hilmi46](https://gitlab.com/hilmi46)
* [@hubermarkus](https://gitlab.com/hubermarkus)
* [@SophiaKunze](https://gitlab.com/SophiaKunze)

### Acknowledgement
This project exists thanks to <a href=https://www.experis.de/de>Experis</a> and our teachers <a href="https://gitlab.com/NicholasLennox">Nicholas Lennox</a> and <a href="https://gitlab.com/sumodevelopment">Dewald Els</a>.

## License
[MIT](https://opensource.org/licenses/MIT) © 2022 Markus Huber, Hilmi Terzi, Cees Paris, and Sophia Kunze
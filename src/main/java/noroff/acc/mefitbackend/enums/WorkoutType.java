package noroff.acc.mefitbackend.enums;

public enum WorkoutType {
    ENDURANCE,
    STRENGTH,
    BALANCE,
    FLEXIBILITY,
    CUSTOM
}

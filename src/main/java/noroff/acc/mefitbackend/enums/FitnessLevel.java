package noroff.acc.mefitbackend.enums;

public enum FitnessLevel {
    BEGINNER,
    INTERMEDIATE,
    ADVANCED
}

package noroff.acc.mefitbackend.enums;

public enum TargetMuscleGroup {
    CHEST,
    BACK,
    ARMS,
    ABDOMINAL,
    LEGS,
    SHOULDERS,
    TRICEPS,
    BICEPS
}

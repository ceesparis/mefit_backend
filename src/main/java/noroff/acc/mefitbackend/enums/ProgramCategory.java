package noroff.acc.mefitbackend.enums;

public enum ProgramCategory {
    INTENSE,
    INTERMEDIATE,
    EASY
}

package noroff.acc.mefitbackend.models.dtos.exercise;

import lombok.Data;
import noroff.acc.mefitbackend.enums.TargetMuscleGroup;
import noroff.acc.mefitbackend.models.dtos.exercise_workout.ExerciseWorkoutDTO;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.ArrayList;
import java.util.Set;

@Data
public class ExercisePostDTO {
    private String name;
    @Enumerated(EnumType.STRING)
    private Set<TargetMuscleGroup> targetMuscleGroups;
    private String description;
    private String imageUrl;
    private String videoUrl;
    // Relationships
    private int creatorId;
    private int lastEditorId;
}

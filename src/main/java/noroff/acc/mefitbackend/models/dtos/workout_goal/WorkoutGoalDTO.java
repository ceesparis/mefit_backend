package noroff.acc.mefitbackend.models.dtos.workout_goal;

import lombok.Data;

@Data
public class WorkoutGoalDTO {
    private int workoutGoalId;
    private int workoutId;
    private boolean isDone;
}

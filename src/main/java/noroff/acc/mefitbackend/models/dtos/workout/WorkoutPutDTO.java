package noroff.acc.mefitbackend.models.dtos.workout;

import lombok.Data;
import noroff.acc.mefitbackend.models.dtos.exercise_workout.ExerciseWorkoutDTO;

import java.util.ArrayList;

@Data
public class WorkoutPutDTO {
    private int id;
    private String name;
    private String type;
    private String description;
    // Relationships
    private int lastEditorId;
    private ArrayList<ExerciseWorkoutDTO> exercises;
}

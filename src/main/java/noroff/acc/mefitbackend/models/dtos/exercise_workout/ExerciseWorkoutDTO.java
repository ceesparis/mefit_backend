package noroff.acc.mefitbackend.models.dtos.exercise_workout;

import lombok.Data;

@Data
public class ExerciseWorkoutDTO {
    private int exerciseId;
    private int sets;
    private int repetitions;
}

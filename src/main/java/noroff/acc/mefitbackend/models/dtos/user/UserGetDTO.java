package noroff.acc.mefitbackend.models.dtos.user;

import lombok.Data;

@Data
public class UserGetDTO {
    private String id;
    private String userName;
    // Relationships
    private int profileId;
}

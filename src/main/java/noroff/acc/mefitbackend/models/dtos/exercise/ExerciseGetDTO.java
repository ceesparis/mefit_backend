package noroff.acc.mefitbackend.models.dtos.exercise;

import lombok.Data;
import noroff.acc.mefitbackend.enums.TargetMuscleGroup;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Set;

@Data
public class ExerciseGetDTO {
    private int id;
    private String name;
    @Enumerated(EnumType.STRING)
    private Set<TargetMuscleGroup> targetMuscleGroups;
    private String description;
    private String imageUrl;
    private String videoUrl;
    // Relationships
    private int lastEditorId;
    private int creatorId;
}

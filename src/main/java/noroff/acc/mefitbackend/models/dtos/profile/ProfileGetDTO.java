package noroff.acc.mefitbackend.models.dtos.profile;

import lombok.Data;
import noroff.acc.mefitbackend.enums.FitnessLevel;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDate;
import java.util.Set;

@Data
public class ProfileGetDTO {
    private int id;
    private String firstName;
    private String lastName;
    private String pictureUrl;
    private int weightKg;
    private int heightCm;
    private LocalDate birthday;
    @Enumerated(EnumType.STRING)
    private FitnessLevel fitnessLevel;
    private Set<Integer> editedWorkouts;
    private Set<Integer> editedPrograms;
    private Set<Integer> editedExercises;
    private Set<Integer> createdWorkouts;
    private Set<Integer> createdPrograms;
    private Set<Integer> createdExercises;
    private Set<Integer> goals;
}

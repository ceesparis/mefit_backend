package noroff.acc.mefitbackend.models.dtos.profile;

import lombok.Data;
import noroff.acc.mefitbackend.enums.FitnessLevel;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDate;

@Data
public class ProfilePostDTO {
    private String firstName;
    private String lastName;
    private String pictureUrl;
    private int weightKg;
    private int heightCm;
    private LocalDate birthday;
    @Enumerated(EnumType.STRING)
    private FitnessLevel fitnessLevel;
}

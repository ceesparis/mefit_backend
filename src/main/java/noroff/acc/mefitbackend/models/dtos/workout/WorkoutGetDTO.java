package noroff.acc.mefitbackend.models.dtos.workout;

import lombok.Data;
import noroff.acc.mefitbackend.models.dtos.exercise_workout.ExerciseWorkoutDTO;

import java.util.Set;

@Data
public class WorkoutGetDTO {
    private int id;
    private String name;
    private Set<String> types;
    private String description;
    // Relationships
    private int lastEditorId;
    private int creatorId;
    private Set<ExerciseWorkoutDTO> exercises;
}

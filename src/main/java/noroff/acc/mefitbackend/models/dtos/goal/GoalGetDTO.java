package noroff.acc.mefitbackend.models.dtos.goal;

import lombok.Data;
import noroff.acc.mefitbackend.models.dtos.workout_goal.WorkoutGoalDTO;

import java.util.Set;

@Data
public class GoalGetDTO {
    private int id;
    private String startDate; // TODO check if this should be a different data type
    // Relationships
    private int profileId;
    private Set<WorkoutGoalDTO> workouts;
}

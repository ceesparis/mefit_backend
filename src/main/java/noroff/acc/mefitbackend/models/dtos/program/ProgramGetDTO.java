package noroff.acc.mefitbackend.models.dtos.program;

import lombok.Data;
import noroff.acc.mefitbackend.enums.ProgramCategory;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Set;

@Data
public class ProgramGetDTO {
    private int id;
    private String name;
    @Enumerated(EnumType.STRING)
    private ProgramCategory category;
    private String description;
    // Relationships
    private int lastEditorId;
    private int creatorId;
    private Set<Integer> workoutIds;
}

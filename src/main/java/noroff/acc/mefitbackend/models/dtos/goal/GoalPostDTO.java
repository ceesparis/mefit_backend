package noroff.acc.mefitbackend.models.dtos.goal;

import lombok.Data;
import noroff.acc.mefitbackend.models.dtos.workout_goal.WorkoutGoalDTO;

import java.time.LocalDate;
import java.util.ArrayList;

@Data
public class GoalPostDTO {
    private LocalDate startDate;
    // Relationships, workouts have to be added by put request
    private int profileId;
}

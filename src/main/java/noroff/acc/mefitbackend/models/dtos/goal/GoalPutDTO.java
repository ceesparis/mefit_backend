package noroff.acc.mefitbackend.models.dtos.goal;

import lombok.Data;
import noroff.acc.mefitbackend.models.dtos.workout_goal.WorkoutGoalDTO;

import java.util.Set;

@Data
public class GoalPutDTO {
    private int id;
    // Relationships
    private Set<WorkoutGoalDTO> workouts;
}

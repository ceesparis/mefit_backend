package noroff.acc.mefitbackend.models.dtos.user;

import lombok.Data;

@Data
public class UserPostDTO {
    private String id;
    private String userName;
}

package noroff.acc.mefitbackend.models.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Set;

@Getter
@Setter
@Entity
public class Goal {

    // Fields
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private LocalDate startDate;

    @Column
    private boolean isDone; // This refers to the goal being done.

    // Relationships
    @ManyToOne
    @JoinColumn(name = "profile_id")
    private Profile profile;

    @OneToMany(mappedBy = "goal")
    private Set<WorkoutGoal> workouts;
}

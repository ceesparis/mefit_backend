package noroff.acc.mefitbackend.models.domain;

import lombok.Getter;
import lombok.Setter;
import noroff.acc.mefitbackend.enums.WorkoutType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Set;

@Getter
@Setter
@Entity
public class Workout {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 50, nullable = false)
    private String name;

    @Column(length = 200)
    private String description;

    @Column(length = 15, nullable = false)
    @ElementCollection
    @Enumerated(EnumType.STRING)
    private Set<WorkoutType> types;

    // Relationships
    @ManyToMany(mappedBy = "workouts")
    private Set<Program> programs;

    @ManyToOne
    @JoinColumn(name = "creator_id")
    private Profile creator;

    @ManyToOne
    @JoinColumn(name = "last_editor_id")
    private Profile lastEditor;

    @OneToMany(mappedBy = "workout")
    private Set<ExerciseWorkout> exercises;

    @OneToMany(mappedBy = "workout")
    private Set<WorkoutGoal> goals;
}

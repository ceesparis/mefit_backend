package noroff.acc.mefitbackend.models.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class WorkoutGoal {

    // Fields
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private boolean isDone; // This refers to a certain workout in a goal being done.

    //Relationships
    @ManyToOne
    @JoinColumn
    private Workout workout;

    @ManyToOne
    @JoinColumn
    private Goal goal;

}

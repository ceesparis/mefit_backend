package noroff.acc.mefitbackend.models.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class AppUser {

    // Fields
    @Id
    private String id;

    @Column(length = 20, nullable = false)
    private String userName;

    // Relationships
    @OneToOne
    @JoinColumn(name = "profile_id")
    private Profile profile;
}

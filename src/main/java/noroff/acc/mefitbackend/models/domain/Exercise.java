package noroff.acc.mefitbackend.models.domain;

import lombok.Getter;
import lombok.Setter;
import noroff.acc.mefitbackend.enums.TargetMuscleGroup;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
public class Exercise {

    // Fields
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 254, nullable = false)
    private String name;

    @Column(length = 200)
    private String description;

    @Column(length = 15, nullable = false)
    @ElementCollection
    @Enumerated(EnumType.STRING)
    private Set<TargetMuscleGroup> targetMuscleGroups;

    @Column(length = 254)
    private String imageUrl;

    @Column(length = 254)
    private String videoUrl;

    // Relationships
    @ManyToOne
    @JoinColumn(name = "creator_id")
    private Profile creator;

    @ManyToOne
    @JoinColumn(name = "last_editor_id")
    private Profile lastEditor;

    @OneToMany(mappedBy = "exercise")
    private Set<ExerciseWorkout> exerciseWorkouts;
}

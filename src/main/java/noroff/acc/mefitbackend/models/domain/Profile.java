package noroff.acc.mefitbackend.models.domain;

import lombok.Getter;
import lombok.Setter;
import noroff.acc.mefitbackend.enums.FitnessLevel;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@Entity
public class Profile {

    // Fields
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 50, nullable = false)
    private String firstName;

    @Column(length = 50, nullable = false)
    private String lastName;

    @Column(length = 500)
    private String pictureUrl;

    @Column
    private int weightKg;

    @Column
    private int heightCm;

    @Column
    private LocalDate birthday;

    @Column(length = 15, nullable = false)
    @Enumerated(EnumType.STRING)
    private FitnessLevel fitnessLevel;

    // Relationships
    @OneToMany(mappedBy = "lastEditor")
    private Set<Workout> editedWorkouts;

    @OneToMany(mappedBy = "lastEditor")
    private Set<Program> editedPrograms;

    @OneToMany(mappedBy = "lastEditor")
    private Set<Exercise> editedExercises;

    @OneToMany(mappedBy = "creator")
    private Set<Workout> createdWorkouts;

    @OneToMany(mappedBy = "creator")
    private Set<Program> createdPrograms;

    @OneToMany(mappedBy = "creator")
    private Set<Exercise> createdExercises;

    @OneToMany(mappedBy = "profile")
    private Set<Goal> goals;

    @OneToOne(mappedBy = "profile")
    private AppUser appUser;

}

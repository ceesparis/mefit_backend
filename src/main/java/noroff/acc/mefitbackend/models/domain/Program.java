package noroff.acc.mefitbackend.models.domain;

import lombok.Getter;
import lombok.Setter;
import noroff.acc.mefitbackend.enums.ProgramCategory;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
public class Program {

    // Fields
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 50)
    private String name;

    @Column(length = 200)
    private String description;

    @Column(length = 15, nullable = false)
    @Enumerated(EnumType.STRING)
    private ProgramCategory category;

    // Relationships
    @ManyToMany
    private Set<Workout> workouts;

    @ManyToOne
    @JoinColumn(name = "creator_id")
    private Profile creator;

    @ManyToOne
    @JoinColumn(name = "last_editor_id")
    private Profile lastEditor;

}

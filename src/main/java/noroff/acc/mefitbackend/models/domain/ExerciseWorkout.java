package noroff.acc.mefitbackend.models.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class ExerciseWorkout {

    // Fields
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private int sets;

    @Column(nullable = false)
    private int repetitions;

    // Relationships
    @ManyToOne
    @JoinColumn
    private Exercise exercise;

    @ManyToOne
    @JoinColumn
    private Workout workout;

}

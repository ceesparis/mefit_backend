package noroff.acc.mefitbackend.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ProgramNotFoundException extends RuntimeException {

    public ProgramNotFoundException(int id) {
        super("Program does not exist with ID: " + id);
    }

    public ProgramNotFoundException(String message) {
        super(message);
    }

    public ProgramNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProgramNotFoundException(Throwable cause) {
        super(cause);
    }

    public ProgramNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}

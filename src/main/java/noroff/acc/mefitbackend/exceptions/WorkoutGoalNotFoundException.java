package noroff.acc.mefitbackend.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class WorkoutGoalNotFoundException extends RuntimeException {
    public WorkoutGoalNotFoundException(int id) {
        super("WorkoutGoal does not exist with ID: " + id);
    }

    public WorkoutGoalNotFoundException(String message) {
        super(message);
    }

    public WorkoutGoalNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public WorkoutGoalNotFoundException(Throwable cause) {
        super(cause);
    }

    public WorkoutGoalNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

package noroff.acc.mefitbackend.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import noroff.acc.mefitbackend.mappers.ExerciseMapper;
import noroff.acc.mefitbackend.models.domain.Exercise;
import noroff.acc.mefitbackend.models.dtos.exercise.ExerciseGetDTO;
import noroff.acc.mefitbackend.models.dtos.exercise.ExercisePostDTO;
import noroff.acc.mefitbackend.models.dtos.exercise.ExercisePutDTO;
import noroff.acc.mefitbackend.services.exercise.ExerciseService;
import noroff.acc.mefitbackend.util.ApiErrorResponse;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@CrossOrigin("*") // TODO specify this later on
@RequestMapping(path = "api/v1/exercises")
public class ExerciseController {

    private final ExerciseMapper exerciseMapper;
    private final ExerciseService exerciseService;

    public ExerciseController(ExerciseMapper exerciseMapper, ExerciseService exerciseService) {
        this.exerciseMapper = exerciseMapper;
        this.exerciseService = exerciseService;
    }


    @Operation(summary = "Get an exercise by it's id.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {@Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = ExerciseGetDTO.class)))}),
            @ApiResponse(
                    responseCode = "404",
                    description = "Exercise does not exist with supplied ID",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("{exercise_id}") // GET localhost:8080/api/v1/exercises/1
    public ResponseEntity<ExerciseGetDTO> findById(@PathVariable int exercise_id) {
        ExerciseGetDTO exercise = exerciseMapper.exerciseToExerciseGetDto(
                exerciseService.findById(exercise_id)
        );
        return ResponseEntity.ok(exercise);
    }

    @Operation(summary = "Get a list of all exercises.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {@Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = ExerciseGetDTO.class)))})
    })
    @GetMapping // GET localhost:8080/api/v1/exercises
    public ResponseEntity<Collection<ExerciseGetDTO>> findAll() {
        Collection<ExerciseGetDTO> exercises = exerciseMapper.exerciseToExerciseGetDto(
                exerciseService.findAll()
        );
        return ResponseEntity.ok(exercises);
    }

    @Operation(summary = "Creates a new exercise.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Exercise successfully created",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))})
    })
    @PostMapping // POST localhost:8080/api/v1/exercises
    public ResponseEntity<?> add(@RequestBody ExercisePostDTO exercisePostDto) {
        Exercise exercise = exerciseMapper.exercisePostDtoToExercise(exercisePostDto);
        Exercise newExercise = exerciseService.add(exercise);
        URI location = URI.create("exercises/" + newExercise.getId());
        return ResponseEntity.created(location).body(location);
    }

    @Operation(summary = "Updates an exercise.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Exercise successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Exercise not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{exercise_id}") // PUT localhost:8080/api/v1/exercise/1
    public ResponseEntity<?> update(@RequestBody ExercisePutDTO exercisePutDto, @PathVariable int exercise_id) {
        if (exercise_id != exercisePutDto.getId())
            return ResponseEntity.badRequest().build();
        exerciseService.update(
                exerciseMapper.exercisePutDtoToExercise(exercisePutDto)
        );
        return ResponseEntity.noContent().build();
    }
//
//    @Operation(summary = "Deletes an exercise.")
//    @DeleteMapping("{exercise_id}") // DELETE localhost:8080/api/v1/exercise/1
//    public ResponseEntity<?> delete(@PathVariable int exercise_id) {
//        //    Deletes an exercise. Contributor only.
//        return null;
//    }
}

package noroff.acc.mefitbackend.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import noroff.acc.mefitbackend.mappers.ProgramMapper;
import noroff.acc.mefitbackend.models.domain.Program;
import noroff.acc.mefitbackend.models.dtos.program.ProgramGetDTO;
import noroff.acc.mefitbackend.models.dtos.program.ProgramPostDTO;
import noroff.acc.mefitbackend.models.dtos.program.ProgramPutDTO;
import noroff.acc.mefitbackend.services.program.ProgramService;
import noroff.acc.mefitbackend.util.ApiErrorResponse;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@CrossOrigin("*") // TODO specify this later on
@RequestMapping(path = "api/v1/programs")
public class ProgramController {

    private final ProgramMapper programMapper;
    private final ProgramService programService;

    public ProgramController(ProgramMapper programMapper, ProgramService programService) {
        this.programMapper = programMapper;
        this.programService = programService;
    }

    @Operation(summary = "Get a program by it's id.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {@Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = ProgramGetDTO.class)))}),
            @ApiResponse(
                    responseCode = "404",
                    description = "Program does not exist with supplied ID",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("{program_id}") // GET localhost:8080/api/v1/programs/1
    public ResponseEntity<ProgramGetDTO> findById(@PathVariable int program_id) {
        ProgramGetDTO program = programMapper.programToProgramGetDto(
                programService.findById(program_id)
        );
        return ResponseEntity.ok(program);
    }

    @Operation(summary = "Get a list of all programs.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {@Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = ProgramGetDTO.class)))})
    })
    @GetMapping // GET localhost:8080/api/v1/programs
    public ResponseEntity<Collection<ProgramGetDTO>> findAll() {
        Collection<ProgramGetDTO> programs = programMapper.programToProgramGetDto(
                programService.findAll()
        );
        return ResponseEntity.ok(programs);
    }

    @Operation(summary = "Creates a new program.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Program successfully created",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))})
    })
    @PostMapping // POST localhost:8080/api/v1/programs
    public ResponseEntity<?> add(@RequestBody ProgramPostDTO programPostDto) {
        Program program = programMapper.programPostDtoToProgram(programPostDto);
        Program newProgram = programService.add(program);
        URI location = URI.create("programs/" + newProgram.getId());
        return ResponseEntity.created(location).body(location);
    }

    @Operation(summary = "Update workouts of a program.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Workouts of program successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Program not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{programId}/workouts")
    public ResponseEntity<?> updateWorkouts(@RequestBody int[] workoutIds, @PathVariable int programId) {
        programService.updateWorkouts(workoutIds, programId);
        return ResponseEntity.noContent().build();
    }


    @Operation(summary = "Executes update of program.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Program successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Program not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{programId}") // PUT localhost:8080/api/v1/program/1
    public ResponseEntity<?> update(@RequestBody ProgramPutDTO programPutDto, @PathVariable int programId) {
        if (programId != programPutDto.getId())
            return ResponseEntity.badRequest().build();
        programService.update(
                programMapper.programPutDtoToProgram(programPutDto)
        );
        return ResponseEntity.noContent().build();
    }
//
//    @Operation(summary = "Deletes an program.")
//    @DeleteMapping("{program_id}") // DELETE localhost:8080/api/v1/program/1
//    public ResponseEntity<?> delete(@PathVariable int program_id) {
//        //    Deletes a program. Contributor only.
//        return null;
//    }
}

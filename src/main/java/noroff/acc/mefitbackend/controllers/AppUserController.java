package noroff.acc.mefitbackend.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import noroff.acc.mefitbackend.mappers.UserMapper;
import noroff.acc.mefitbackend.models.domain.AppUser;
import noroff.acc.mefitbackend.models.domain.Profile;
import noroff.acc.mefitbackend.models.dtos.user.UserGetDTO;
import noroff.acc.mefitbackend.models.dtos.user.UserPostDTO;
import noroff.acc.mefitbackend.services.profile.ProfileService;
import noroff.acc.mefitbackend.services.user.UserService;
import noroff.acc.mefitbackend.util.ApiErrorResponse;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@CrossOrigin("*") // TODO specify this later on
@RequestMapping(path = "api/v1/users")
public class AppUserController {

    private final UserMapper userMapper;
    private final UserService userService;
    private final ProfileService profileService;

    public AppUserController(UserMapper userMapper, UserService userService, ProfileService profileService) {
        this.userMapper = userMapper;
        this.userService = userService;
        this.profileService = profileService;
    }

    @Operation(summary = "Get a user by exact matching name.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = UserGetDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "User does not exist with supplied name",
                    content = { @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })

    @GetMapping("{userName}") // GET GET localhost:8080/api/v1/users/1
    public ResponseEntity<UserGetDTO> findByName(@PathVariable String userName) {
        UserGetDTO user = userMapper.userToUserGetDto(
                userService.findByName(userName)
        );
        return ResponseEntity.ok(user);
    }

    @Operation(summary = "Creates a new user.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "User successfully created",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))})
    })
    @PostMapping // POST localhost:8080/api/v1/users
    public ResponseEntity<?> add(@RequestBody UserPostDTO userPostDto) {
        AppUser appUser = userMapper.userPostDtoToUser(userPostDto);
        AppUser newUser = userService.add(appUser);
        URI location = URI.create("users/" + newUser.getId());
        return ResponseEntity.created(location).body(
                userMapper.userToUserGetDto(newUser)
        );
    }

    @Operation(summary = "Link a profile to a user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Profile successfully linked to user.",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request. Make sure to link a profile that is not linked to any other user.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))}),
            @ApiResponse(responseCode = "404",
                    description = "User or profile not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{userId}/profile") // PUT localhost:8080/api/v1/goals/1/workouts
    public ResponseEntity<?> updateProfile(@RequestBody int profileId, @PathVariable String userId) {
        // Check if profile is already linked to a user
        Profile profile = profileService.findById(profileId);
        if (profile.getAppUser() != null)
            return ResponseEntity.badRequest().build();
        userService.updateProfile(profileId, userId);
        return ResponseEntity.noContent().build();
    }
}

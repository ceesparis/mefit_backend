package noroff.acc.mefitbackend.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import noroff.acc.mefitbackend.mappers.WorkoutMapper;
import noroff.acc.mefitbackend.models.dtos.workout.WorkoutGetDTO;
import noroff.acc.mefitbackend.services.workout.WorkoutService;
import noroff.acc.mefitbackend.util.ApiErrorResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@CrossOrigin("*") // TODO specify this later on
@RequestMapping(path = "api/v1/workouts")
public class WorkoutController {

    private final WorkoutMapper workoutMapper;
    private final WorkoutService workoutService;

    public WorkoutController(WorkoutMapper workoutMapper, WorkoutService workoutService) {
        this.workoutMapper = workoutMapper;
        this.workoutService = workoutService;
    }

    @Operation(summary = "Get a workout by it's id.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {@Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = WorkoutGetDTO.class)))}),
            @ApiResponse(
                    responseCode = "404",
                    description = "Workout does not exist with supplied ID",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("{workout_id}") // GET localhost:8080/api/v1/workouts/1
    public ResponseEntity<WorkoutGetDTO> findById(@PathVariable int workout_id) {
        WorkoutGetDTO workout = workoutMapper.workoutToWorkoutGetDto(
                workoutService.findById(workout_id)
        );
        return ResponseEntity.ok(workout);
    }

    @Operation(summary = "Get a list of all workouts.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {@Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = WorkoutGetDTO.class)))})
    })
    @GetMapping // GET localhost:8080/api/v1/workouts
    public ResponseEntity<Collection<WorkoutGetDTO>> findAll() {
        Collection<WorkoutGetDTO> workouts = workoutMapper.workoutToWorkoutGetDto(
                workoutService.findAll()
        );
        return ResponseEntity.ok(workouts);
    }

//    @Operation(summary = "Creates a new workout.")
//    @PostMapping // POST localhost:8080/api/v1/workouts
//    public ResponseEntity<?> add(@RequestBody WorkoutPostDTO workoutPostDto) {
//        //    Creates a new workout. Accepts appropriate parameters in the request body as application/json. Contributor only.
//        return null;
//    }

//    @Operation(summary = "Executes a partial update of the workout corresponding to the provided workout_id.")
//    @PutMapping("{workout_id}") // PUT localhost:8080/api/v1/workouts/1
//    public ResponseEntity<?> update(@RequestBody WorkoutPutDTO workoutPutDto, @PathVariable int workout_id) {
//        //    Executes a partial update of the workout corresponding to the provided workout_id.
//        //    Accepts appropriate parameters in the request body as application/json. Contributor only.
//        //    If an unauthorized person attempts to update a workout then the server should respond with 403 Forbidden.
//        return null;
//    }
//
//    @Operation(summary = "Deletes a workout.")
//    @DeleteMapping("{workout_id}") // DELETE localhost:8080/api/v1/workouts/1
//    public ResponseEntity<?> delete(@PathVariable int workout_id) {
//        //    Deletes a workout. Accepts appropriate parameters in the request body as application/json. Contributor only.
//        //    Deleting a workout may only be done by a contributor and can only delete workouts that they have contributed.
//        //    If an unauthorized person attempts to delete a workout then the server should respond with 403 Forbidden.
//        return null;
//    }
}

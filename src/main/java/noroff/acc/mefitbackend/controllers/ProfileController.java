package noroff.acc.mefitbackend.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import noroff.acc.mefitbackend.mappers.ProfileMapper;
import noroff.acc.mefitbackend.models.domain.Profile;
import noroff.acc.mefitbackend.models.dtos.profile.ProfileGetDTO;
import noroff.acc.mefitbackend.models.dtos.profile.ProfilePostDTO;
import noroff.acc.mefitbackend.models.dtos.profile.ProfilePutDTO;
import noroff.acc.mefitbackend.services.profile.ProfileService;
import noroff.acc.mefitbackend.util.ApiErrorResponse;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@CrossOrigin("*") // TODO specify this later on
@RequestMapping(path = "api/v1/profiles")
public class ProfileController {

    private final ProfileMapper profileMapper;
    private final ProfileService profileService;

    public ProfileController(ProfileMapper profileMapper, ProfileService profileService) {
        this.profileMapper = profileMapper;
        this.profileService = profileService;
    }

    @Operation(summary = "Get a profile by it's id.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {@Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = ProfileGetDTO.class)))}),
            @ApiResponse(
                    responseCode = "404",
                    description = "Profile does not exist with supplied ID",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("{profile_id}") // GET localhost:8080/api/v1/profiles/1
    public ResponseEntity<ProfileGetDTO> findById(@PathVariable int profile_id) {
        ProfileGetDTO profile = profileMapper.profileToProfileGetDto(
                profileService.findById(profile_id)
        );
        return ResponseEntity.ok(profile);
    }

    @Operation(summary = "Creates a new profile.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Profile successfully created",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))})
    })
    @PostMapping // POST localhost:8080/api/v1/profiles
    public ResponseEntity<?> add(@RequestBody ProfilePostDTO profilePostDto) {
        Profile profile = profileMapper.profilePostDtoToProfile(profilePostDto);
        Profile newProfile = profileService.add(profile);
        URI location = URI.create("profiles/" + newProfile.getId());
        return ResponseEntity.created(location).body(location);
    }

    @Operation(summary = "Updates a profile.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Profile successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Profile not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{profile_id}") // PUT localhost:8080/api/v1/profiles/1
    public ResponseEntity<?> update(@RequestBody ProfilePutDTO profilePutDto, @PathVariable int profile_id) {
        if (profile_id != profilePutDto.getId())
            return ResponseEntity.badRequest().build();
        profileService.update(
                profileMapper.profilePutDtoToProfile(profilePutDto)
        );
        return ResponseEntity.noContent().build();
    }
//
//    @Operation(summary = "Deletes a profile. User only")
//    @DeleteMapping("{profile_id}") // DELETE localhost:8080/api/v1/profile/1
//    public ResponseEntity<?> delete(@PathVariable int profile_id) {
//        //    Deletes a profile. User only
//        return null;
//    }

}

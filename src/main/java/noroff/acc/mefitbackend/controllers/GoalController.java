package noroff.acc.mefitbackend.controllers;

import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import noroff.acc.mefitbackend.mappers.GoalMapper;
import noroff.acc.mefitbackend.models.domain.Goal;
import noroff.acc.mefitbackend.models.dtos.goal.GoalGetDTO;
import noroff.acc.mefitbackend.models.dtos.goal.GoalPostDTO;
import noroff.acc.mefitbackend.services.goal.GoalService;
import noroff.acc.mefitbackend.util.ApiErrorResponse;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.v3.oas.annotations.Operation;

import java.net.URI;

@RestController
@CrossOrigin("*") // TODO specify this later on
@RequestMapping(path = "api/v1/goals")
public class GoalController {

    private final GoalMapper goalMapper;
    private final GoalService goalService;



    public GoalController(GoalMapper goalMapper, GoalService goalService) {
        this.goalMapper = goalMapper;
        this.goalService = goalService;
    }

    @Operation(summary = "Get a goal by it's id.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {@Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = GoalGetDTO.class)))}),
            @ApiResponse(
                    responseCode = "404",
                    description = "Goal does not exist with supplied ID",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("{goal_id}") // GET localhost:8080/api/v1/goals/1
    public ResponseEntity<GoalGetDTO> findById(@PathVariable int goal_id) {
        GoalGetDTO goal = goalMapper.goalToGoalGetDTO(
                goalService.findById(goal_id)
        );
        return ResponseEntity.ok(goal);
    }

    @Operation(summary = "Creates a new goal assigned to a profile. Workouts of this goal are to be added by a separate endpoint.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Goal successfully created",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))})
    })
    @PostMapping // POST localhost:8080/api/v1/goals
    public ResponseEntity<?> add(@RequestBody GoalPostDTO goalPostDto) {
        Goal goal = goalMapper.goalPostDtoToGoal(goalPostDto);
        Goal newGoal = goalService.add(goal);
        URI location = URI.create("goals/" + newGoal.getId());
        return ResponseEntity.created(location).body(location);
    }

    @Operation(summary = "Updates status of a workout in a goal. Make sure to specify the workout_goal_id and NOT the id of the workout.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Status of workout in a goal successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Workout_goal not found with supplied ID",
                    content = @Content)
    })
    @PutMapping({"{workout_goal_id}"}) // PUT localhost:8080/api/v1/workout_goals/1
    public ResponseEntity<?> updateWorkoutStatus(@RequestBody boolean isDone, @PathVariable int workout_goal_id) {
        goalService.updateWorkoutStatus(workout_goal_id, isDone);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Adds a new workout to a goal. IsDone status of the workout is set to false.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Workout successfully added to goal.",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Goal or workout not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{goal_id}/workouts") // PUT localhost:8080/api/v1/goals/1/workouts
    public ResponseEntity<?> addWorkouts(@RequestBody int workoutId, @PathVariable int goal_id) {
        goalService.addWorkout(workoutId, goal_id);
        return ResponseEntity.noContent().build();
    }

    // TODO implement deletion of goal
//    @Operation(summary = "This should delete a goal of the corresponding goal_id")
//    @DeleteMapping("{goal_id}") // DELETE localhost:8080/api/v1/goals/1
//    public ResponseEntity<?> delete(@PathVariable int goal_id) {
//        //    This should delete a goal of the corresponding goal_id
//        return null;
//    }
}
package noroff.acc.mefitbackend.services.exercise;

import noroff.acc.mefitbackend.exceptions.ExerciseNotFoundException;
import noroff.acc.mefitbackend.models.domain.Exercise;
import noroff.acc.mefitbackend.repositories.ExerciseRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class ExerciseServiceImpl implements ExerciseService {

    private final ExerciseRepository exerciseRepository;

    public ExerciseServiceImpl(ExerciseRepository exerciseRepository) {
        this.exerciseRepository = exerciseRepository;
    }

    //    T findById(ID id); // GET
    @Override
    public Exercise findById(Integer id) {
        return exerciseRepository.findById(id)
                .orElseThrow(() -> new ExerciseNotFoundException(id));
    }

    //    Collection<T> findAll(); // GET
    @Override
    public Collection<Exercise> findAll() {
        return exerciseRepository.findAll();
    }

    //    T add(T entity); // POST
    @Override
    public Exercise add(Exercise exercise) {
        return exerciseRepository.save(exercise);
    }

    //    T update(T entity); // PUT
    @Override
    public Exercise update(Exercise exercise) {
        // Check if profile exists
        int exerciseId = exercise.getId();

        if (!exerciseRepository.existsById(exerciseId))
            throw new ExerciseNotFoundException(exerciseId);
        return exerciseRepository.save(exercise);
    }

    //    void deleteById(ID id); // DELETE
    @Override
    public void deleteById(Integer id) {
        // TODO implement
    }

}

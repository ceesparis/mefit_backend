package noroff.acc.mefitbackend.services.exercise;

import noroff.acc.mefitbackend.models.domain.Exercise;
import noroff.acc.mefitbackend.services.CrudService;

public interface ExerciseService extends CrudService<Exercise, Integer> {
}

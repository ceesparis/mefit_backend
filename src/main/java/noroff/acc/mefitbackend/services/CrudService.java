package noroff.acc.mefitbackend.services;

import java.util.Collection;

public interface CrudService<T, ID> {

    T findById(ID id); // GET

    Collection<T> findAll(); // GET

    T add(T entity); // POST

    T update(T entity); // PUT

    void deleteById(ID id); // DELETE
}

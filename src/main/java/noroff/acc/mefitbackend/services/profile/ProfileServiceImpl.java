package noroff.acc.mefitbackend.services.profile;

import noroff.acc.mefitbackend.exceptions.ProfileNotFoundException;
import noroff.acc.mefitbackend.models.domain.Profile;
import noroff.acc.mefitbackend.repositories.ProfileRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class ProfileServiceImpl implements ProfileService {

    private final ProfileRepository profileRepository;

    public ProfileServiceImpl(ProfileRepository profileRepository) {
        this.profileRepository = profileRepository;
    }

    //    T findById(ID id); // GET
    @Override
    public Profile findById(Integer id) {
        return profileRepository.findById(id)
                .orElseThrow(() -> new ProfileNotFoundException(id));
    }
    //    Collection<T> findAll(); // GET
    @Override
    public Collection<Profile> findAll() {
        // TODO implement
        return null;
    }

    //    T add(T entity); // POST
    @Override
    public Profile add(Profile profile) {
        return profileRepository.save(profile);
    }

    //    T update(T entity); // PUT
    @Override
    public Profile update(Profile profile) {
        // check if profile exists
        int profileId = profile.getId();
        if (!profileRepository.existsById(profileId))
            throw new ProfileNotFoundException(profileId);
        return profileRepository.save(profile);
    }

    //    void deleteById(ID id); // DELETE
    @Override
    public void deleteById(Integer id) {
        // TODO implement
    }
}

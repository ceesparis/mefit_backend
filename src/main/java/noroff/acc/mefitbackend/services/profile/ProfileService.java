package noroff.acc.mefitbackend.services.profile;

import noroff.acc.mefitbackend.models.domain.Profile;
import noroff.acc.mefitbackend.services.CrudService;

public interface ProfileService extends CrudService<Profile, Integer> {
}

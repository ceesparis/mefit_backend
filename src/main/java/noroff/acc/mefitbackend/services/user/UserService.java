package noroff.acc.mefitbackend.services.user;

import noroff.acc.mefitbackend.models.domain.AppUser;

public interface UserService {
    AppUser findByName(String userName);
    AppUser add(AppUser appUser);

    AppUser updateProfile(int profileId, String userId);
}

package noroff.acc.mefitbackend.services.user;

import noroff.acc.mefitbackend.exceptions.ExerciseNotFoundException;
import noroff.acc.mefitbackend.exceptions.ProfileNotFoundException;
import noroff.acc.mefitbackend.models.domain.AppUser;
import noroff.acc.mefitbackend.repositories.ProfileRepository;
import noroff.acc.mefitbackend.repositories.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ProfileRepository profileRepository;

    public UserServiceImpl(UserRepository userRepository, ProfileRepository profileRepository) {
        this.userRepository = userRepository;
        this.profileRepository = profileRepository;
    }

    @Override
    public AppUser findByName(String userName) {
        return userRepository.findByName(userName);
    }

    @Override
    public AppUser add(AppUser user) {
        return userRepository.save(user);
    }

    @Override
    public AppUser updateProfile(int profileId, String userId) {
        // Receive user.
        AppUser user = userRepository.findById(userId)
                .orElseThrow(() -> new ExerciseNotFoundException(userId));
        // Set profile of user.
        user.setProfile(profileRepository.findById(profileId)
                .orElseThrow(() -> new ProfileNotFoundException(profileId)));
        return userRepository.save(user);
    }

}

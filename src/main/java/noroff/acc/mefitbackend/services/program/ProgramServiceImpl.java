package noroff.acc.mefitbackend.services.program;

import noroff.acc.mefitbackend.exceptions.ProgramNotFoundException;
import noroff.acc.mefitbackend.exceptions.WorkoutNotFoundException;
import noroff.acc.mefitbackend.models.domain.Program;
import noroff.acc.mefitbackend.models.domain.Workout;
import noroff.acc.mefitbackend.repositories.ProgramRepository;
import noroff.acc.mefitbackend.repositories.WorkoutRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Service
public class ProgramServiceImpl implements ProgramService {

    public final ProgramRepository programRepository;
    public final WorkoutRepository workoutRepository;

    public ProgramServiceImpl(ProgramRepository programRepository, WorkoutRepository workoutRepository) {
        this.programRepository = programRepository;
        this.workoutRepository = workoutRepository;
    }

    //    T findById(ID id); // GET
    @Override
    public Program findById(Integer id) {
        return programRepository.findById(id)
                .orElseThrow(() -> new ProgramNotFoundException(id));
    }

    //    Collection<T> findAll(); // GET
    @Override
    public Collection<Program> findAll() {
        return programRepository.findAll();
    }

    //    T add(T entity); // POST
    @Override
    public Program add(Program program) {
        return programRepository.save(program);
    }

    //    T update(T entity); // PUT
    @Override
    public Program update(Program program) {
        int programId = program.getId();
        // Get current workouts (they should remain the same)
        Program oldProgram = programRepository.findById(programId)
                .orElseThrow(() -> new ProgramNotFoundException(programId));
        Set<Workout> workouts = oldProgram.getWorkouts();
        program.setWorkouts(workouts);
        if (!programRepository.existsById(programId)) {
            throw new ProgramNotFoundException(programId);
        }
        return programRepository.save(program);
    }

    @Override
    public Program updateWorkouts(int[] workoutIds, int programId) {
        // Receive Program.
        Program program = programRepository.findById(programId)
                .orElseThrow(() -> new ProgramNotFoundException(programId));
        Set<Workout> workouts = new HashSet<>();
        for (int id : workoutIds) {
            if (workoutRepository.existsById(id)) {
                // Receive workout by id.
                workouts.add(workoutRepository.findById(id)
                        .orElseThrow(() -> new WorkoutNotFoundException(id)));
            }
        }
        program.setWorkouts(workouts);
        return programRepository.save(program);
    }

    //    void deleteById(ID id); // DELETE
    @Override
    public void deleteById(Integer id) {
        // TODO implement
    }
}

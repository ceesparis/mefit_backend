package noroff.acc.mefitbackend.services.program;

import noroff.acc.mefitbackend.models.domain.Program;
import noroff.acc.mefitbackend.services.CrudService;

public interface ProgramService extends CrudService<Program, Integer> {
    Program updateWorkouts(int[] workoutIds, int programId);
}

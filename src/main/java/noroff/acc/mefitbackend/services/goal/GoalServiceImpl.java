package noroff.acc.mefitbackend.services.goal;

import noroff.acc.mefitbackend.exceptions.GoalNotFoundException;
import noroff.acc.mefitbackend.exceptions.WorkoutGoalNotFoundException;
import noroff.acc.mefitbackend.exceptions.WorkoutNotFoundException;
import noroff.acc.mefitbackend.models.domain.Goal;
import noroff.acc.mefitbackend.models.domain.WorkoutGoal;
import noroff.acc.mefitbackend.repositories.GoalRepository;
import noroff.acc.mefitbackend.repositories.WorkoutGoalRepository;
import noroff.acc.mefitbackend.repositories.WorkoutRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class GoalServiceImpl implements GoalService {

    private final GoalRepository goalRepository;
    private final WorkoutRepository workoutRepository;
    private final WorkoutGoalRepository workoutGoalRepository;

    public GoalServiceImpl(GoalRepository goalRepository, WorkoutRepository workoutRepository, WorkoutGoalRepository workoutGoalRepository) {
        this.goalRepository = goalRepository;
        this.workoutRepository = workoutRepository;
        this.workoutGoalRepository = workoutGoalRepository;
    }

    //    T findById(ID id); // GET
    @Override
    public Goal findById(Integer id) {
        return goalRepository.findById(id)
                .orElseThrow(() -> new GoalNotFoundException(id));
    }

    //    Collection<T> findAll(); // GET
    @Override
    public Collection<Goal> findAll() {
        // TODO implement
        return null;
    }

    //    T add(T entity); // POST
    @Override
    public Goal add(Goal goal) {
        return goalRepository.save(goal);
    }

    //    T update(T entity); // PUT
    @Override
    public Goal update(Goal goal) {
        // TODO implement
        return null;
    }

    @Override
    public WorkoutGoal addWorkout(int workoutId, int goalId) {
        // Receive goal.
        Goal goal = goalRepository.findById(goalId)
                .orElseThrow(() -> new GoalNotFoundException(goalId));
        // Create new workoutGoal.
        WorkoutGoal workoutGoal = new WorkoutGoal();
        // In the beginning a workout is not done.
        workoutGoal.setDone(false);
        // Set workout of workoutGoal.
        workoutGoal.setWorkout(workoutRepository.findById(workoutId)
                .orElseThrow(() -> new WorkoutNotFoundException(workoutId)));
        // Set goal of workoutGoal.
        workoutGoal.setGoal(goal);
        // Add workoutGoal.
        return workoutGoalRepository.save(workoutGoal);
    }

    @Override
    public WorkoutGoal updateWorkoutStatus(int workoutGoalId, boolean isDone) {
        // Receive workoutGoal
        WorkoutGoal workoutGoal = workoutGoalRepository.findById(workoutGoalId)
                .orElseThrow(() -> new WorkoutGoalNotFoundException(workoutGoalId));
        // set status
        workoutGoal.setDone(isDone);
        workoutGoalRepository.save(workoutGoal);
        return workoutGoal;
    }

    //    void deleteById(ID id); // DELETE
    @Override
    public void deleteById(Integer id) {
        // TODO implement
    }
}

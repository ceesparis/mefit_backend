package noroff.acc.mefitbackend.services.goal;

import noroff.acc.mefitbackend.models.domain.Goal;
import noroff.acc.mefitbackend.models.domain.WorkoutGoal;
import noroff.acc.mefitbackend.services.CrudService;

public interface GoalService extends CrudService<Goal, Integer> {
    WorkoutGoal addWorkout(int workoutId, int goalId);
    WorkoutGoal updateWorkoutStatus(int workoutGoalId, boolean isDone);
}

package noroff.acc.mefitbackend.services.workout;

import noroff.acc.mefitbackend.exceptions.WorkoutNotFoundException;
import noroff.acc.mefitbackend.models.domain.Workout;
import noroff.acc.mefitbackend.repositories.WorkoutRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class WorkoutServiceImpl implements WorkoutService {

    private final WorkoutRepository workoutRepository;

    public WorkoutServiceImpl(WorkoutRepository workoutRepository) {
        this.workoutRepository = workoutRepository;
    }

    //    T findById(ID id); // GET
    @Override
    public Workout findById(Integer id) {
        return workoutRepository.findById(id)
                .orElseThrow(() -> new WorkoutNotFoundException(id));
    }

    //    Collection<T> findAll(); // GET
    @Override
    public Collection<Workout> findAll() {
        return workoutRepository.findAll();
    }

    //    T add(T entity); // POST
    @Override
    public Workout add(Workout workout) {
        // TODO implement
        return null;
    }

    //    T update(T entity); // PUT
    @Override
    public Workout update(Workout workout) {
        // TODO implement
        return null;
    }

    //    void deleteById(ID id); // DELETE
    @Override
    public void deleteById(Integer id) {
        // TODO implement
    }
}

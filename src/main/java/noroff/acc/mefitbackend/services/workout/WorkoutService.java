package noroff.acc.mefitbackend.services.workout;

import noroff.acc.mefitbackend.models.domain.Workout;
import noroff.acc.mefitbackend.services.CrudService;

public interface WorkoutService extends CrudService<Workout, Integer> {
}

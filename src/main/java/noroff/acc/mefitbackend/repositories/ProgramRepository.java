package noroff.acc.mefitbackend.repositories;

import noroff.acc.mefitbackend.models.domain.Program;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProgramRepository extends JpaRepository<Program, Integer> {
}

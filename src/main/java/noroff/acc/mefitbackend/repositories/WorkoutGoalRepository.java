package noroff.acc.mefitbackend.repositories;

import noroff.acc.mefitbackend.models.domain.Program;
import noroff.acc.mefitbackend.models.domain.WorkoutGoal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkoutGoalRepository extends JpaRepository<WorkoutGoal, Integer> {

}

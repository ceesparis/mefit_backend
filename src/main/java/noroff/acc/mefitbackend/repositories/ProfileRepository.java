package noroff.acc.mefitbackend.repositories;

import noroff.acc.mefitbackend.models.domain.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfileRepository extends JpaRepository<Profile, Integer> {
}

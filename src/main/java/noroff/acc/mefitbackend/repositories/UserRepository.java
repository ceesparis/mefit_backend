package noroff.acc.mefitbackend.repositories;

import noroff.acc.mefitbackend.models.domain.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<AppUser, Object> {

    @Query("SELECT user from AppUser user where user.userName LIKE ?1")
    AppUser findByName(String userName);
    }


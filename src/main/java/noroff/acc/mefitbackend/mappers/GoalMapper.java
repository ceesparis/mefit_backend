package noroff.acc.mefitbackend.mappers;

import noroff.acc.mefitbackend.models.domain.Goal;
import noroff.acc.mefitbackend.models.domain.Profile;
import noroff.acc.mefitbackend.models.domain.WorkoutGoal;
import noroff.acc.mefitbackend.models.dtos.goal.GoalGetDTO;
import noroff.acc.mefitbackend.models.dtos.goal.GoalPostDTO;
import noroff.acc.mefitbackend.models.dtos.workout_goal.WorkoutGoalDTO;
import noroff.acc.mefitbackend.services.profile.ProfileService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class GoalMapper {

    @Autowired
    WorkoutGoalMapper workoutGoalMapper;
    @Autowired
    ProfileService profileService;

    // goalToGoalGetDto (overloaded)
    @Mapping(target = "profileId", source = "profile.id")
    @Mapping(target = "workouts", source = "workouts", qualifiedByName = "workoutsToWorkouts")
    public abstract GoalGetDTO goalToGoalGetDTO(Goal goal);

    // goalPostDtoToGoal
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "workouts", ignore = true)
    @Mapping(target = "profile", source = "profileId", qualifiedByName = "profileIdToProfile")
    public abstract Goal goalPostDtoToGoal(GoalPostDTO goalPostDto);

    @Named("workoutsToWorkouts")
    Set<WorkoutGoalDTO> mapWorkoutsToWorkouts(Set<WorkoutGoal> workouts) {
        if (workouts == null)
            return null;
        return workouts.stream()
                .map(workout -> this.workoutGoalMapper.workoutGoalToWorkoutGoalDTO(workout))
                .collect(Collectors.toSet());
    }

    @Named("profileIdToProfile")
    Profile mapProfileIdToProfile(int profileId) {
        if (profileId == 0)
            return null;
        return profileService.findById(profileId);
    }


    // goalPutDtoToGoal


}

package noroff.acc.mefitbackend.mappers;

import noroff.acc.mefitbackend.models.domain.*;
import noroff.acc.mefitbackend.models.dtos.profile.ProfileGetDTO;
import noroff.acc.mefitbackend.models.dtos.profile.ProfilePostDTO;
import noroff.acc.mefitbackend.models.dtos.profile.ProfilePutDTO;
import noroff.acc.mefitbackend.services.profile.ProfileService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class ProfileMapper {

    @Autowired
    ProfileService profileService;

    // profileToProfileGetDto (overloaded)
    @Mapping(target = "editedWorkouts", source = "editedWorkouts", qualifiedByName = "editedWorkoutsToEditedWorkoutsIds")
    @Mapping(target = "editedPrograms", source = "editedPrograms", qualifiedByName = "editedProgramsToEditedProgramsIds")
    @Mapping(target = "editedExercises", source = "editedExercises", qualifiedByName = "editedExercisesToEditedExercisesIds")
    @Mapping(target = "createdWorkouts", source = "createdWorkouts", qualifiedByName = "createdWorkoutsToCreatedWorkoutsIds")
    @Mapping(target = "createdPrograms", source = "createdPrograms", qualifiedByName = "createdProgramsToCreatedProgramsIds")
    @Mapping(target = "createdExercises", source = "createdExercises", qualifiedByName = "createdExercisesToCreatedExercisesIds")
    @Mapping(target = "goals", source = "goals", qualifiedByName = "goalsToGoalsIds")
    public abstract ProfileGetDTO profileToProfileGetDto(Profile profile);

//    @Mapping(target = "editedWorkouts", source = "editedWorkouts", qualifiedByName = "editedWorkoutIdsToEditedWorkouts")
//    @Mapping(target = "editedPrograms", source = "editedPrograms", qualifiedByName = "editedProgramIdsToEditedPrograms")
//    @Mapping(target = "editedExercises", source = "editedExercises", qualifiedByName = "editedExerciseIdsToEditedExercises")
//    @Mapping(target = "createdWorkouts", source = "createdWorkouts", qualifiedByName = "createdWorkoutIdsToCreatedWorkouts")
//    @Mapping(target = "createdPrograms", source = "createdPrograms", qualifiedByName = "createdProgramIdsToCreatedPrograms")
//    @Mapping(target = "createdExercises", source = "createdExercises", qualifiedByName = "createdExerciseIdsToCreatedExercises")
    @Mapping(target = "id", ignore = true)
    public abstract Profile profilePostDtoToProfile(ProfilePostDTO profilePostDto);

    public abstract Profile profilePutDtoToProfile(ProfilePutDTO profilePutDto);


    @Named("editedWorkoutsToEditedWorkoutsIds")
    Set<Integer> mapEditedWorkoutsToEditedWorkoutsIds(Set<Workout> editedWorkouts) {
        if (editedWorkouts == null)
            return null;
        return editedWorkouts.stream()
                .map(Workout::getId)
                .collect(Collectors.toSet());
    }

    @Named("editedProgramsToEditedProgramsIds")
    Set<Integer> mapEditedProgramsToEditedProgramsIds(Set<Program> editedPrograms) {
        if (editedPrograms == null)
            return null;
        return editedPrograms.stream()
                .map(Program::getId)
                .collect(Collectors.toSet());
    }

    @Named("editedExercisesToEditedExercisesIds")
    Set<Integer> mapEditedExercisesToEditedExercisesIds(Set<Exercise> editedExercises) {
        if (editedExercises == null)
            return null;
        return editedExercises.stream()
                .map(Exercise::getId)
                .collect(Collectors.toSet());
    }

    @Named("createdWorkoutsToCreatedWorkoutsIds")
    Set<Integer> mapCreatedWorkoutsToCreatedWorkoutsIds(Set<Workout> createdWorkouts) {
        if (createdWorkouts == null)
            return null;
        return createdWorkouts.stream()
                .map(Workout::getId)
                .collect(Collectors.toSet());
    }

    @Named("createdProgramsToCreatedProgramsIds")
    Set<Integer> mapCreatedProgramsToCreatedProgramsIds(Set<Program> createdPrograms) {
        if (createdPrograms == null)
            return null;
        return createdPrograms.stream()
                .map(Program::getId)
                .collect(Collectors.toSet());
    }

    @Named("createdExercisesToCreatedExercisesIds")
    Set<Integer> mapCreatedExercisesToCreatedExercisesIds(Set<Exercise> createdExercises) {
        if (createdExercises == null)
            return null;
        return createdExercises.stream()
                .map(Exercise::getId)
                .collect(Collectors.toSet());
    }

    @Named("goalsToGoalsIds")
    Set<Integer> MapGoalsToGoalsIds(Set<Goal> goals) {
        if (goals == null)
            return null;
        return goals.stream()
                .map(Goal::getId)
                .collect(Collectors.toSet());
    }

    // profilePutDtoToProfile

    // profilePostDtoToProfile
}

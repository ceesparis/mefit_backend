package noroff.acc.mefitbackend.mappers;

import noroff.acc.mefitbackend.models.domain.AppUser;
import noroff.acc.mefitbackend.models.dtos.user.UserGetDTO;
import noroff.acc.mefitbackend.models.dtos.user.UserPostDTO;
import noroff.acc.mefitbackend.services.user.UserService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class UserMapper {

    @Autowired
    UserService userService;

    // userToUserGetDto
    @Mapping(target = "profileId", source = "profile.id")
    public abstract UserGetDTO userToUserGetDto(AppUser user);

    public abstract AppUser userPostDtoToUser(UserPostDTO user);
}

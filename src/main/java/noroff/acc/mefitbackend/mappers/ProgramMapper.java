package noroff.acc.mefitbackend.mappers;

import noroff.acc.mefitbackend.models.domain.Profile;
import noroff.acc.mefitbackend.models.domain.Program;
import noroff.acc.mefitbackend.models.domain.Workout;
import noroff.acc.mefitbackend.models.dtos.program.ProgramGetDTO;
import noroff.acc.mefitbackend.models.dtos.program.ProgramPostDTO;
import noroff.acc.mefitbackend.models.dtos.program.ProgramPutDTO;
import noroff.acc.mefitbackend.services.profile.ProfileService;
import noroff.acc.mefitbackend.services.program.ProgramService;
import noroff.acc.mefitbackend.services.workout.WorkoutService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class ProgramMapper {

    @Autowired
    ProgramService programService;
    @Autowired
    ProfileService profileService;
    @Autowired
    WorkoutService workoutService;


    // programToProgramGetDto (overloaded)
    @Mapping(target = "lastEditorId", source = "lastEditor.id")
    @Mapping(target = "creatorId", source = "creator.id")
    @Mapping(target = "workoutIds", source = "workouts", qualifiedByName = "workoutsToWorkoutIds")
    public abstract ProgramGetDTO programToProgramGetDto(Program program);

    @Mapping(target = "lastEditorId", source = "lastEditor.id")
    @Mapping(target = "creatorId", source = "creator.id")
    @Mapping(target = "workoutIds", source = "workouts", qualifiedByName = "workoutsToWorkoutIds")
    public abstract Collection<ProgramGetDTO> programToProgramGetDto(Collection<Program> programs);

    // programPostDtoToProgram
    @Mapping(target = "creator", source = "creatorId", qualifiedByName = "profileIdToProfile")
    @Mapping(target = "lastEditor", source = "lastEditorId", qualifiedByName = "profileIdToProfile")
    public abstract Program programPostDtoToProgram(ProgramPostDTO programPostDto);

    // programPutDtoToProgram
    @Mapping(target = "creator", source = "creatorId", qualifiedByName = "profileIdToProfile")
    @Mapping(target = "lastEditor", source = "lastEditorId", qualifiedByName = "profileIdToProfile")
    public abstract Program programPutDtoToProgram(ProgramPutDTO programPutDto);

    @Named("workoutsToWorkoutIds")
    Set<Integer> mapWorkoutsToWorkoutIds(Set<Workout> workouts) {
        if (workouts == null)
            return null;
        return workouts.stream()
                .map(Workout::getId)
                .collect(Collectors.toSet());
    }

    @Named("profileIdToProfile")
    Profile mapProfileIdToProfile(int profileId) {
        if (profileId == 0)
            return null;
        return profileService.findById(profileId);
    }

}

package noroff.acc.mefitbackend.mappers;

import noroff.acc.mefitbackend.models.domain.WorkoutGoal;
import noroff.acc.mefitbackend.models.dtos.workout_goal.WorkoutGoalDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public abstract class WorkoutGoalMapper {

    @Mapping(target = "workoutId", source = "workout.id")
    @Mapping(target = "workoutGoalId", source = "id")
    public abstract WorkoutGoalDTO workoutGoalToWorkoutGoalDTO(WorkoutGoal workout);
}

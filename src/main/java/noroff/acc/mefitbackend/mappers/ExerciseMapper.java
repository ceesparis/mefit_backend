package noroff.acc.mefitbackend.mappers;

import noroff.acc.mefitbackend.enums.TargetMuscleGroup;
import noroff.acc.mefitbackend.models.domain.Exercise;
import noroff.acc.mefitbackend.models.domain.Profile;
import noroff.acc.mefitbackend.models.dtos.exercise.ExerciseGetDTO;
import noroff.acc.mefitbackend.models.dtos.exercise.ExercisePostDTO;
import noroff.acc.mefitbackend.models.dtos.exercise.ExercisePutDTO;
import noroff.acc.mefitbackend.services.exercise.ExerciseService;
import noroff.acc.mefitbackend.services.profile.ProfileService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class ExerciseMapper {

    @Autowired
    protected ProfileService profileService;

    // exerciseToExerciseGetDto (overloaded)
    @Mapping(target = "lastEditorId", source = "lastEditor.id")
    @Mapping(target = "creatorId", source = "creator.id")
    public abstract ExerciseGetDTO exerciseToExerciseGetDto(Exercise exercise);

    @Mapping(target = "lastEditorId", source = "lastEditor.id")
    @Mapping(target = "creatorId", source = "creator.id")
    public abstract Collection<ExerciseGetDTO> exerciseToExerciseGetDto(Collection<Exercise> exercises);

    // exercisePostDtoToExercise
    @Mapping(target = "creator", source = "creatorId", qualifiedByName = "profileIdToProfile")
    @Mapping(target = "lastEditor", source = "lastEditorId", qualifiedByName = "profileIdToProfile")
    @Mapping(target = "id", ignore = true)
    public abstract Exercise exercisePostDtoToExercise(ExercisePostDTO exercisePostDto);

    // exercisePutDtoToExercise
    @Mapping(target = "creator", source = "creatorId", qualifiedByName = "profileIdToProfile")
    @Mapping(target = "lastEditor", source = "lastEditorId", qualifiedByName = "profileIdToProfile")
    public abstract Exercise exercisePutDtoToExercise(ExercisePutDTO exercisePutDto);

    @Named("profileIdToProfile")
    Profile mapProfileIdToProfile(int profileId) {
        if (profileId == 0)
            return null;
        return profileService.findById(profileId);
    }
}

package noroff.acc.mefitbackend.mappers;
import noroff.acc.mefitbackend.enums.WorkoutType;
import noroff.acc.mefitbackend.models.domain.ExerciseWorkout;
import noroff.acc.mefitbackend.models.domain.Workout;
import noroff.acc.mefitbackend.models.dtos.exercise_workout.ExerciseWorkoutDTO;
import noroff.acc.mefitbackend.models.dtos.workout.WorkoutGetDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;


@Mapper(componentModel = "spring")
public abstract class WorkoutMapper {

    @Autowired
    ExerciseWorkoutMapper exerciseWorkoutMapper;

//     workoutToWorkoutGetDto (overloaded)
    @Mapping(target = "lastEditorId", source = "lastEditor.id")
    @Mapping(target = "creatorId", source = "creator.id")
    @Mapping(target = "exercises", source = "exercises", qualifiedByName = "exercisesToExercises")
    @Mapping(target = "types", source = "types", qualifiedByName = "typesToTypes")
    public abstract WorkoutGetDTO workoutToWorkoutGetDto(Workout workout);

    @Mapping(target = "lastEditorId", source = "lastEditor.id")
    @Mapping(target = "creatorId", source = "creator.id")
    @Mapping(target = "exercises", source = "exercises", qualifiedByName = "exercisesToExercises")
    @Mapping(target = "types", source = "types", qualifiedByName = "typesToTypes")
    public abstract Collection<WorkoutGetDTO> workoutToWorkoutGetDto(Collection<Workout> workouts);

    @Named("exercisesToExercises")
    Set<ExerciseWorkoutDTO> mapExercisesToExercises(Set<ExerciseWorkout> exercises) {
        if (exercises == null)
            return null;
        return exercises.stream()
                .map(exercise -> this.exerciseWorkoutMapper.exerciseWorkoutToExerciseWorkoutDTO(exercise))
                .collect(Collectors.toSet());
    }

    @Named("typesToTypes")
    Set<String> mapTypesToTypes(Set<WorkoutType> types) {
        if (types == null)
            return null;
        return types.stream()
                .map(WorkoutType::toString)
                .collect(Collectors.toSet());
    }

    // workoutPutDtoToWorkout

    // workoutPostDtoToWorkout
}

package noroff.acc.mefitbackend.mappers;

import noroff.acc.mefitbackend.models.domain.ExerciseWorkout;
import noroff.acc.mefitbackend.models.dtos.exercise_workout.ExerciseWorkoutDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public abstract class ExerciseWorkoutMapper {

    @Mapping(target = "exerciseId", source = "exercise.id")
    public abstract ExerciseWorkoutDTO exerciseWorkoutToExerciseWorkoutDTO(ExerciseWorkout exercise);
}

-- Updated upstream
-- profile
INSERT INTO profile (birthday, first_name, fitness_level, height_cm, last_name, weight_kg) VALUES ('1990-01-05', 'Donald', 'ADVANCED', 180, 'Duck', 80); -- id 1
INSERT INTO profile (birthday, first_name, fitness_level, height_cm, last_name, weight_kg) VALUES ('1992-04-13', 'Crispy', 'BEGINNER', 165, 'Curl', 73); -- id 2
INSERT INTO profile (birthday, first_name, fitness_level, height_cm, last_name, weight_kg) VALUES ('1990-01-05', 'Minky', 'ADVANCED', 180, 'Winky', 80); -- id 3
INSERT INTO profile (birthday, first_name, fitness_level, height_cm, last_name, weight_kg) VALUES ('1992-04-13', 'Willy', 'BEGINNER', 165, 'Chilly', 73); -- id 4
INSERT INTO profile (birthday, first_name, fitness_level, height_cm, last_name, weight_kg) VALUES ('1990-01-05', 'Fitty', 'ADVANCED', 180, 'Fat', 80); -- id 5

-- exercise
INSERT INTO exercise (description, name, image_url, video_url , last_editor_id, creator_id) VALUES ('This exercise will pump the heck out of your Six packs.','Sit-Up','https://www.foodspring.co.uk/magazine/wp-content/uploads/2020/11/Sit-ups-Endposition-%C2%A9Geber86-3.jpg','https://www.youtube.com/watch?v=1fbU_MkV7NE', 1, 1); -- id 1
INSERT INTO exercise (description, name, image_url, video_url ,last_editor_id, creator_id) VALUES ('This exercise will pump the heck out of your chest arms and back','Push-Up', 'https://madbarzpictures.blob.core.windows.net/madbarzpictures/push-up-hold.jpg','https://www.youtube.com/watch?v=IODxDxX7oi4', 1, 2); -- id 2
INSERT INTO exercise (description, name, image_url, video_url ,last_editor_id, creator_id) VALUES ('This exercise will pump the heck out of your chest.', 'Wide push-up','https://madbarzpictures.blob.core.windows.net/madbarzpictures/wide_push-up.jpg','https://www.youtube.com/watch?v=rr6eFNNDQdU', 2, 3); -- id 3
INSERT INTO exercise (description, name, image_url, video_url ,last_editor_id, creator_id) VALUES ('This exercise will pump the heck out of your legs.', 'Squats','https://post.healthline.com/wp-content/uploads/2019/03/Female_Squat_Studio_732x549-thumbnail-2.jpg','https://www.youtube.com/watch?v=xqvCmoLULNY', 2, 1); -- id 4
INSERT INTO exercise (description, name, image_url, video_url ,last_editor_id, creator_id) VALUES ('This exercise will pump the heck out of your Biceps.', 'pull-up', 'https://www.newbodyplan.co.uk/wp-content/uploads/2022/02/chin-up-back-biceps-strength-pull-up-muscle.jpg','https://www.youtube.com/watch?v=6Lz7ys1xRdo',2, 3); -- id 5
INSERT INTO exercise (description, name, image_url, video_url ,last_editor_id, creator_id) VALUES ('This exercise will pump the heck out of your Triceps.', 'Triceps dips','https://i.pinimg.com/originals/3d/c3/7b/3dc37bb9988899324270ce02cecfa8bd.png','https://www.youtube.com/watch?v=jdFzYGmvDyg', 2, 1); -- id 6
INSERT INTO exercise (description, name, image_url, video_url ,last_editor_id, creator_id) VALUES ('This exercise will pump the heck out of your back.', 'Aquaman','https://image.scoopwhoop.com/w620/s3.scoopwhoop.com/anj/image/c2e69898-71bc-4608-847d-f0146cc19fa7.jpg.webp','https://www.youtube.com/watch?v=MmEr4oFH2SQ', 2, 1); -- id 7

INSERT INTO exercise_target_muscle_groups (exercise_id, target_muscle_groups) VALUES (1, 'ABDOMINAL');
INSERT INTO exercise_target_muscle_groups (exercise_id, target_muscle_groups) VALUES (2, 'CHEST');
INSERT INTO exercise_target_muscle_groups (exercise_id, target_muscle_groups) VALUES (2, 'ARMS');
INSERT INTO exercise_target_muscle_groups (exercise_id, target_muscle_groups) VALUES (2, 'BACK');

INSERT INTO exercise_target_muscle_groups (exercise_id, target_muscle_groups) VALUES (3, 'CHEST');
INSERT INTO exercise_target_muscle_groups (exercise_id, target_muscle_groups) VALUES (4, 'LEGS');
INSERT INTO exercise_target_muscle_groups (exercise_id, target_muscle_groups) VALUES (5, 'BICEPS');
INSERT INTO exercise_target_muscle_groups (exercise_id, target_muscle_groups) VALUES (6, 'TRICEPS');
INSERT INTO exercise_target_muscle_groups (exercise_id, target_muscle_groups) VALUES (7, 'BACK');

INSERT INTO workout (description,name, last_editor_id, creator_id) VALUES ('Have you finished a month of workouts? Come and experience something else. ','Tear And Sweat', 4, 1); -- id 1
INSERT INTO workout (description,name, last_editor_id, creator_id) VALUES ('A beginner workout that you could take. We do not want to scare you away!','I just started', 4, 4); -- id 2
INSERT INTO workout (description, name, last_editor_id, creator_id) VALUES ('This workout will pump the heck out of your body.', 'Pump It Up', 1, 1); -- id 3
INSERT INTO workout (description, name, last_editor_id, creator_id) VALUES ('My special holiday workout.', 'Persistence', 2, 1); -- id 4


INSERT INTO workout_types (workout_id, types) VALUES (1, 'STRENGTH');
INSERT INTO workout_types (workout_id, types) VALUES (2, 'STRENGTH');
INSERT INTO workout_types (workout_id, types) VALUES (2, 'ENDURANCE');

-- exercise_workout
-- Workout ID 1
INSERT INTO exercise_workout (repetitions, sets, exercise_id, workout_id) VALUES (10, 3, 1, 1);


INSERT INTO exercise_workout (repetitions, sets, exercise_id, workout_id) VALUES (12, 3, 2, 1);
INSERT INTO exercise_workout (repetitions, sets, exercise_id, workout_id) VALUES (10, 3, 3, 1);

-- Workout ID 2
INSERT INTO exercise_workout (repetitions, sets, exercise_id, workout_id) VALUES (10, 3, 4, 2);
INSERT INTO exercise_workout (repetitions, sets, exercise_id, workout_id) VALUES (12, 3, 5, 2);
INSERT INTO exercise_workout (repetitions, sets, exercise_id, workout_id) VALUES (12, 3, 2, 2);
-- Workout ID 3
INSERT INTO exercise_workout (repetitions, sets, exercise_id, workout_id) VALUES (10, 3, 5, 3);
INSERT INTO exercise_workout (repetitions, sets, exercise_id, workout_id) VALUES (12, 3, 4, 3);
INSERT INTO exercise_workout (repetitions, sets, exercise_id, workout_id) VALUES (8, 3, 3, 3);
INSERT INTO exercise_workout (repetitions, sets, exercise_id, workout_id) VALUES (6, 3, 2, 3);
INSERT INTO exercise_workout (repetitions, sets, exercise_id, workout_id) VALUES (12, 3, 1, 3);

-- Workoout ID 4
INSERT INTO exercise_workout (repetitions, sets, exercise_id, workout_id) VALUES (10, 3, 4, 4);
INSERT INTO exercise_workout (repetitions, sets, exercise_id, workout_id) VALUES (12, 3, 5, 4);
INSERT INTO exercise_workout (repetitions, sets, exercise_id, workout_id) VALUES (10, 3, 2, 4);

-- program
INSERT INTO program (name, category, description, last_editor_id, creator_id) VALUES ('Pump your upper body', 'INTERMEDIATE','To feel much better', 1, 1); -- id 1
INSERT INTO program (name, category, description, last_editor_id, creator_id) VALUES ('Go Fast', 'EASY', 'The best cardio workout you will ever get.', 1, 2); -- id 2
INSERT INTO program (name, category, description, last_editor_id, creator_id) VALUES ('Go Hard', 'INTENSE' , 'The best Strength workout you will ever get', 2, 1); -- id 3

-- workout_programs
INSERT INTO program_workouts (workouts_id, programs_id) VALUES (1, 1);
INSERT INTO program_workouts (workouts_id, programs_id) VALUES (2, 2);
INSERT INTO program_workouts (workouts_id, programs_id) VALUES (3, 3);
INSERT INTO program_workouts (workouts_id, programs_id) VALUES (4, 3);

-- goal
INSERT INTO goal (is_done, start_date, profile_id) VALUES (false, '2022-06-09', 1); -- id 1
INSERT INTO goal (is_done, start_date, profile_id) VALUES (true, '2022-06-16', 1); -- id 2
INSERT INTO goal (is_done, start_date, profile_id) VALUES (false, '2022-06-09', 2); -- id 3

-- workout_goal
INSERT INTO workout_goal (is_done, goal_id, workout_id) VALUES (false, 1, 1); -- id 1
INSERT INTO workout_goal (is_done, goal_id, workout_id) VALUES (true, 1, 2); -- id 2

-- user
-- INSERT INTO app_user (user_name, password) VALUES ('sophia', 'gH@T#PHXvocW') -- id 1
-- =======
-- >>>>>>> Stashed changes

